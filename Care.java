public interface Care {
    //permet de prendre soin d’un patient pas d'implementation par default
    void careForPatient(Patient patient);
    //implémentation par défaut qui affiche le contenu de la note
    default void recordPatientVisit(String notes) {
        System.out.println("Notes enregistrées : " + notes);
    }
}

class Nurse extends MedicalStaff {
    public Nurse(String name, int age, String socialSecurityNumber, String employeeId) {
        super(name, age, socialSecurityNumber, employeeId);
    }

    @Override
    //getRole() : affiche "Nurse"
    public String getRole() {
        return "Infirmière";
    }

    @Override
    //careForPatient(Patient patient) : affiche "Nurse $nurseName cares for $patientName"
    public void careForPatient(Patient patient) {
        System.out.println(getRole() + " " + super.getName() + " prend soin de " + patient.getName());
    }
}


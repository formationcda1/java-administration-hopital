public class Hospital {
    public static void main(String[] args) {
        Medication aspirine = new Medication("Aspirine", "500mg");
        Illness grippe = new Illness("Grippe");
        grippe.addMedication(aspirine);

        Medication doliprane = new Medication("Doliprane", "1000mg");
        Illness fievre = new Illness ("Fièvre");
        fievre.addMedication(doliprane);

        Medication smecta = new Medication("Smecta", "1 sachet");
        Illness gastro = new Illness("Gastro");
        gastro.addMedication(smecta);

         Patient patient1 = new Patient("Alice", 30, "123-45-6789", "P001");
        patient1.addIllness(grippe);

        Patient patient2 = new Patient("Robert", 50, "123-45-1234", "P002");
        patient2.addIllness(gastro);
        
        System.out.println(patient1.getInfo());

        Doctor doctor1 = new Doctor("Dr. Dupont", 45, "987-65-4321", "D001", "Cardiologie");
        doctor1.careForPatient(patient1);
    }
   
}
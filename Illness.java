import java.util.ArrayList;
//Cette classe représente une maladie avec un nom et une liste de médicaments associés.
public class Illness {
    String name;
    ArrayList<Medication> medicationList;

    public Illness (String name){
        this.name = name;
        //crée une liste vide de médicament
        this.medicationList= new ArrayList<>();
    }
    
    public void addMedication(Medication medication){
        medicationList.add(medication);
    }
    //getInfo() : retourne les informations sur la maladie et la liste des informations des médicaments
    public String getInfo(){
        StringBuilder info = new StringBuilder("Maladie : " + name + "\nMédicament : ");
        //boucle for qui parcourt la liste des médicaments associés à la maladie (stockée dans medicationList)
        for (Medication medication : medicationList){
            // ajoute les informations obtenues de medication.getInfo()) à la chaîne info
            info.append(medication.getInfo()).append("\n");
        } 
        //info.toString() renvoie la chaîne de caractères  construite par StringBuilder
        return info.toString();
    }




}
import java.util.ArrayList;

public class Patient extends Person {
    private String patientId;
    private ArrayList<Illness> illnessList;

    public Patient(String name, int age, String string, String patientId) {
    //super appelle le constructeur de la classe parente Person ( abstraite) pour initialiser name, age et socialSecurityNumbe 
    super(name, age, string);
    this.patientId = patientId;
    //crée une liste vide d’objets Illness (maladies) pour stocker les maladies associées au patient
    this.illnessList = new ArrayList<>();
        
    }
    //addIllness(Illness illness) : ajoute une maladie à la liste du patient
    public void addIllness (Illness illness){
         illnessList.add(illness);
    }

    //getInfo() : retourne les informations du patient (nom, âge, numéro de sécurité sociale et les informations sur ses maladies)
    public String getInfo(){
        StringBuilder info = new StringBuilder("Nom : " + super.getName() + " " + "Âge : " + super.getAge() + "\nNuméro de sécurité sociale : " + super.getSocialSecurityNumber() + "\nMaladies : \n");
        for (Illness illness : illnessList){
            info.append(illness.getInfo()).append("\n");
        }
        return info.toString();
    }





}

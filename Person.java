public class Person {
    String name;
    int age;
    String socialSecurityNumber;

    //contructeur Person  
    public Person( String name, int age, String socialSecurityNumber){
        this.name = name;
        this.age = age;
        this.socialSecurityNumber = socialSecurityNumber;
    }

    public String getName(){
        return name;
    }

    public int getAge(){
        return age;
    }
    
    public String getSocialSecurityNumber(){
        return socialSecurityNumber;
    }
}

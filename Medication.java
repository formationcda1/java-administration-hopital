public class Medication {
    String name;
    String dosage;

    public Medication(String name, String dosage) {
        this.name = name;
        this.dosage = dosage;
    }

    public String getInfo(){
        return ( name + "\nDosage : " + dosage);
    }
}

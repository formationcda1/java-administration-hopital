public class Doctor extends MedicalStaff {
    private String specialty;
    
    public Doctor ( String name, int age, String socialSecurityNumber, String employeeId, String specialty){
        super(name, age, socialSecurityNumber, employeeId);
        this.specialty = specialty;
    }
    //@Override pour indiquer le remplacement des méthodes getRole et careForPatient(Patient patient) de la classe parente MedicalStaff
   
    @Override
     //retourne le rôle du médecin, qui est “Médecin”
    public String getRole() {
        return "Médecin";
    }
    
    @Override
    //méthode prend un paramètre patient
    public void careForPatient(Patient patient) {
        //ffiche un message indiquant que le médecin prend soin du patient en utilisant le nom du médecin et le nom du patient.
        System.out.println(getRole() + " " + super.getName() + " prend soin de " + patient.getName());
    }

}
